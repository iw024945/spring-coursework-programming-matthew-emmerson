#include "Room.h"
#include "Item.h"
#include "Character.h"
Room::Room(const std::string& desc)
{
	this->description = desc;
}

void Room::AddItem(const Item& item)
{
	items.push_back(item);
}




void Room::RemoveItem(const Item& item)
{
	for (int i = 0; i < items.size(); i++)
	{
		if (items[i].GetName() == item.GetName())
		{
			items.erase(items.begin() + i);
			break;
		}
	}
}

std::string Room::GetDescription()
{
	return description;
}

std::vector<Item> Room::GetItems()
{

	return items;
	
}

Room* Room::GetExit(const std::string& direction)
{
		return exits[direction];
	
}

void Room::AddExit(const std::string& direction, Room* room)
{
	exits[direction] = room;
	exits[direction] = room;
}

void Room::SetDescription(const std::string& desc)
{
	description = desc;
}

void Room::AddEnemy(Enemy* enemy)
{
	enemies.push_back(enemy);
}




