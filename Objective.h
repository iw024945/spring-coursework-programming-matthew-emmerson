#pragma once
#include <string>
#include "Item.h"




class Objective {
public:
    //the possible statuses of the quest
    enum Status { NotStarted, InProgress, Completed };
    Status status;
    std::string description;

    Objective(const std::string& description)
        : status(NotStarted), description(description) {}

    virtual void start() {
        status = InProgress;
    }

    virtual void complete() {
        status = Completed;
    }

    virtual bool checkCompletion() = 0; 
};
//inherit to create the kill target quest
class KillObjective : public Objective {
private:
    int targetCount;
    int currentCount;
public:
    KillObjective(const std::string& description, int targetCount)
        : Objective(description), targetCount(targetCount), currentCount(0) {}
    //increase the kill counter
    void incrementKillCount() {
        if (status == InProgress) {
            currentCount++;
        }
    }
    //win condition
    bool checkCompletion() override {
        return currentCount >= targetCount;
    }
};
//inherit the fetch class
class FetchObjective : public Objective {
private:
    Item targetItem;
public:
    FetchObjective(const std::string& description, const Item& targetItem)
        : Objective(description), targetItem(targetItem) {}
    //set the item to be fetched
    void setItemFetched() {
        if (status == InProgress) {
            complete();
        }
    }
    //win condition
    bool checkCompletion() override {
        return status == Completed;
    }
};
