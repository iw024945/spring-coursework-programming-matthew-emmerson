#pragma once
#include <iostream>
#include <string>
#include <vector>
#include "Objective.h"

class Quest {
public:
    enum Status { NotStarted, InProgress, Completed };
    Status status;
    std::string name;
    std::vector<Objective*> objectives;

    Quest(const std::string& name, const std::vector<Objective*>& objectives)
        : status(NotStarted), name(name), objectives(objectives) {}

    

    void displayObjectives() const {
        for (const Objective* objective : objectives) {
            std::cout << "- " << objective->description << " (";
            switch (objective->status) {
            case Objective::NotStarted:
                std::cout << "Not Started";
                break;
            case Objective::InProgress:
                std::cout << "In Progress";
                break;
            case Objective::Completed:
                std::cout << "Completed";
                break;
            }
            std::cout << ")" << std::endl;
        }
    }
 

    void start() {
        status = InProgress;
        for (Objective* objective : objectives) {
            objective->start();
        }
    }

    void update() {
        // Check if all objectives are completed
        bool allCompleted = true;
        for (const Objective* objective : objectives) {
            if (objective->status != Objective::Completed) {
                allCompleted = false;
                break;
            }
        }

        // If all objectives are completed, complete the quest
        if (allCompleted) {
            complete();
        }
    }


    void complete() {
        status = Completed;
    }
};
