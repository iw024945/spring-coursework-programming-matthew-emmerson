#include "CommandInterpreter.h"
#include "Area.h"
#include "Room.h"
#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
#include <vector>
#include <map>
#include "Character.h"
#include "Item.h"
#include "Quest.h"


CommandInterpreter::CommandInterpreter(Player* player, const std::string& Command)
{
	//split the command into words
	std::vector<std::string> words;
	std::istringstream iss(Command);
	std::string word;
	while (iss >> word)
	{
		words.push_back(word);
	}


	//check if the first word is "move"
	if (words[0] == "move")
	{
		//check if the second word is a valid direction
		if (words[1] == "north" || words[1] == "south" || words[1] == "east" || words[1] == "west")
		{
			//move the player in the specified direction
			//get the player's current location
			Room* currentRoom = player->GetLocation();
			//get the exit in the specified direction
			Room* newRoom = currentRoom->GetExit(words[1]);
			//if the exit exists, move the player to the new room
			if (newRoom != nullptr)
			{
				player->SetLocation(newRoom);
				std::cout << "You move " << words[1] << std::endl;
				std::cout << newRoom->GetDescription() << std::endl;
				

			}
			else
			{
				std::cout << "There is no exit in that direction." << std::endl;
			}
		}
		else
		{
			std::cout << "Invalid direction." << std::endl;
		}
	}
	else if (words[0] == "look")
	{
		//get the player's current location
		Room* currentRoom = player->GetLocation();
		//print the room description
		std::cout << currentRoom->GetDescription() << std::endl;
	}
	else if (words[0] == "take")
	{
		//get the player's current location
		Room* currentRoom = player->GetLocation();
		//get the items in the room
		std::vector<Item> items = currentRoom->GetItems();
		//check if the specified item is in the room
		bool itemFound = false;
		for (Item item : items)
		{
			if (item.GetName() == words[1])
			{
				//add the item to the player's inventory
				player->AddItem(item);
				//remove the item from the room
				currentRoom->RemoveItem(item);
				itemFound = true;
				std::cout << "You take the " << item.GetName() << "." << std::endl;
				break;
			}
		}	
		if (!itemFound)
		{
			std::cout << "That item is not in this room." << std::endl;
		}

		
	}
	else if (words[0] == "Quests") {
		// Get the player's quests
		std::vector<Quest>& quests = player->getQuests();
		// Display objectives for each quest
		for (const Quest& quest : quests) {
			quest.displayObjectives();
		}
		

	}
	else
	{
		std::cout << "Invalid command." << std::endl;
	}
	
}


