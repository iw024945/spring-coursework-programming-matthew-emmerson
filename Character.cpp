#include "Character.h"
#include "Room.h"
#include "Item.h"

Character::Character(const std::string& name, int health) {
	this->name = name;
	this->health = health;
}


void Character::TakeDamage(int damage)
{
	health -= damage;
}

void Character::combat(Character& opponent) {
	while (health > 0 && opponent.health > 0) {
		
			int damage = (weapon != nullptr) ? weapon->damage : 10; // Use weapon damage if a weapon is equipped, otherwise use a default value
			opponent.TakeDamage(damage); // This character hits opponent
			if (opponent.health > 0) {
				TakeDamage(10);
			
			}

		
	}
}

void Character::equipWeapon(Weapon* weapon)
{
	this->weapon = weapon;
}

Player::Player(const std::string& name, int health) : Character(name, health)
{
	
}
void Player::SetLocation(Room* room)
{
	location = room;
}




Room* Player::GetLocation()
{
	return location;
}

void Character::AddItem(const Item& item)
{
	inventory.push_back(item);
}
//function to handle a character moving rooms. it needs to work for both npcs and players
void Character::Move(const std::string& direction)
{
	//get the current room
	Room* currentRoom = location;
	//get the room in the direction the character wants to move
	Room* newRoom = currentRoom->GetExit(direction);
	//if the room is not null, move the character to the new room
	if (newRoom != nullptr)
	{
		location = newRoom;
	}
}




void Rogue::SneakStrike(Character& opponent)
{

	if (this->weapon) {
		int damage = weapon->getDamage();
		opponent.TakeDamage(damage);
		std::cout << "You used slash on " << opponent.getName() << " for " << damage << " damage" << ".\n";
		opponent.TakeDamage(damage/2);
		std::cout << "You used stab on " << opponent.getName() << " for " << damage/2 << " damage" << ".\n";
	}
}

void Mage::Spell(Character& opponent)
{
	if (this->weapon) {
		int damage = weapon->getDamage();
		opponent.TakeDamage(damage);
		health += damage / 2; // Heal for half the damage dealt
		std::cout << "You used Spell on " << opponent.getName() << " for " << damage << " damage and healed for " << damage / 2 << ".\n";
	}
}

void Warrior::PowerStrike(Character& opponent)
{
	if (this->weapon) {
		int damage = weapon->getDamage() * 2; // Double damage
		opponent.TakeDamage(damage);
		std::cout << "You used Power Strike on " << opponent.getName() << " for " << damage << " damage.\n";
	}
}
