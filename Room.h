#pragma once
#include <iostream>
#include <string>
#include <map>
#include <vector>
#include "Item.h"

class Enemy;
class Room {
private:
	std::string description;
	std::vector<Item> items;
	std::map<std::string, Room*> exits;
	std::vector<Enemy*> enemies; 

public:
	Room(const std::string& desc);
	void AddItem(const Item& item);
	void RemoveItem(const Item& item);
	std::string GetDescription();
	std::vector<Item> GetItems();
	Room* GetExit(const std::string& direction);
	void AddExit(const std::string& direction, Room* room);
	void SetDescription(const std::string& desc);
	void AddEnemy(Enemy* enemy);
};

