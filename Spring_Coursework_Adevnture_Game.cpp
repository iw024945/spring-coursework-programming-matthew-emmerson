#include <iostream>
#include <string>
#include <map>
#include <vector>
#include "Item.h"
#include "Room.h"
#include "Character.h"
#include "Area.h"
#include "CommandInterpreter.h"
int main() {
	Area area;
	area.LoadMapFromFile("game_map.txt"); 

	//// Create a Player
	//Player player("Alice", 100);
	//// Set the player's starting location
	//player.SetLocation(area.GetRoom("Room1")); 
	////create an item
	//Item test("string", "Some cool string");
	////output details about the item
	//std::cout << "Item name: " << test.GetName() << std::endl;
	//std::cout << "Item description: " << test.GetDescription() << std::endl;
	////output player loc description
	//std::cout << "Player's location: " << player.GetLocation()->GetDescription() << std::endl;
	////give the hex value for the room south of player
	//std::cout << "Exit North:: " << player.GetLocation()->GetExit("south") << std::endl;
	////add room 2 to the east
	//player.GetLocation()->AddExit("East",area.GetRoom("Room2"));
	////show its hex value
	//std::cout << "Exist East: " << player.GetLocation()->GetExit("East") << std::endl;

















	Player player("Alice", 100);
	player.SetLocation(area.GetRoom("Room1"));

	Objective* findKey = new FetchObjective("Find the golden key", Item("Golden Key", "A shiny golden key."));
	Objective* defeatDragon = new KillObjective("Defeat the dragon", 1);
	std::vector<Objective*> objectives = { findKey, defeatDragon };


	// Create quest
	Quest quest("The Dragon's Lair", objectives);

	// Add quest to player
	player.addQuest(quest);

	// Game loop (basic interaction)
	while (true) {
		std::cout << "Current Location: " << player.GetLocation() -> GetDescription() << std::endl;
		std::cout << "Items in the room:" << std::endl;
		for (const Item& item : player.GetLocation()->GetItems()) {
			std::cout << "- " << item.GetName() << ": " <<item.GetDescription() << std::endl;
		}
		// use the CommandInterpreter to get the player's command
		std::string command;
		std::cout << "Enter a command: " << std::endl;
		std::cout << "Enter move,look or take. followed by the target " << std::endl;
		std::cout << "for example: move east OR take key " << std::endl;
		
	
		std::getline(std::cin, command);

		CommandInterpreter interpreter(&player, command);



		
		}
	}
	

