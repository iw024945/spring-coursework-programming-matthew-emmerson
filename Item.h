#pragma once
#include <iostream>
#include <string>

class Character;


class Item {
private:
	std::string name;
	std::string description;
public:
	Item(const std::string& name, const std::string& desc);
	void Interact(Character& player);
	std::string GetName() const;
	std::string GetDescription() const;
};


