#pragma once
#include <iostream>
#include <string>
#include <map>
#include "Room.h"
class Area
{
private:
	std::map<std::string, Room*> rooms;
public:
	void AddRoom(const std::string& name, Room* room);
	Room* GetRoom(const std::string& name);
	void ConnectRooms(const std::string& room1Name, const std::string& room2Name, const std::string& direction);
	void LoadMapFromFile(const std::string& filename);


};

