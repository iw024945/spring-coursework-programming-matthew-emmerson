#include "Area.h"
#include "Room.h"
#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
#include "Weapon.h"
#include "Character.h"

void Area::AddRoom(const std::string& name, Room* room)
{
	rooms.insert(std::pair<std::string, Room*>(name, room));
}

Room* Area::GetRoom(const std::string& name)
{
	return rooms[name];
	return rooms[name];
}

void Area::ConnectRooms(const std::string& room1Name, const std::string& room2Name, const std::string& direction)
{
	Room* room1 = GetRoom(room1Name);
	Room* room2 = GetRoom(room2Name);
	room1->AddExit(direction, room2);
	
}

void Area::LoadMapFromFile(const std::string& filename)
{
    std::ifstream file(filename);
    if (!file) {
        std::cerr << "Unable to open file " << filename << std::endl;
        return;
    }

    std::string line;
    std::map<std::string, std::vector<std::string>> connections;
    while (std::getline(file, line)) {
        std::istringstream iss(line);
        std::string roomName, roomDescription;
        std::getline(iss, roomName, ':'); // read room name until ':'
        std::getline(iss, roomDescription, ':'); // read room description until ':'

        // create room if it doesn't exist
        if (rooms.find(roomName) == rooms.end()) {
            rooms[roomName] = new Room(roomDescription);
        }

        // process items
        std::string itemData;
        std::getline(iss, itemData, ':'); // read items until ':'
        std::istringstream issItems(itemData);
        std::string itemName, itemDescription;
        while (issItems >> itemName >> itemDescription) { // read item name and description
            Item item(itemName, itemDescription);
            rooms[roomName]->AddItem(item);
        }

        // store connections for later processing
        std::string connection;
        while (std::getline(iss, connection, ',')) { // read connections until ','
            connections[roomName].push_back(connection);
        }
        std::string enemyData;
        std::getline(iss, enemyData, ':'); // read enemies until ':'
        std::istringstream issEnemies(enemyData);
        std::string enemyName, weaponName;
        int enemyHealth, weaponDamage;
        while (issEnemies >> enemyName >> enemyHealth >> weaponName >> weaponDamage) { // read enemy name, health, weapon name and damage
            Weapon* weapon = new Weapon(weaponName, weaponDamage);
            Enemy* enemy = new Enemy(enemyName, enemyHealth, weapon);
            rooms[roomName]->AddEnemy(enemy);
        }

    }

    // process connections
    for (const auto& pair : connections) {
        const std::string& roomName = pair.first;
        const std::vector<std::string>& connectionData = pair.second;
        for (const std::string& connection : connectionData) {
            std::istringstream iss(connection);
            std::string connectedRoomName, direction;
            iss >> connectedRoomName >> direction; // read room name and direction
            ConnectRooms(roomName, connectedRoomName, direction);
        }
    }

    file.close();
}






