#include "Item.h"
#include "Character.h"
Item::Item(const std::string& name, const std::string& desc)
{
	this->name = name;
	this->description = desc;
}

void Item::Interact(Character& player) {
    std::cout << "You interact with the " << name << std::endl;
    std::cout << description << std::endl;
    std::cout << "Would you like to pick up?(yes or no)" << std::endl;
    std::string input;
    std::cin >> input;
    if (input == "yes") {
        std::cout << "You pick up the " << name << std::endl;
        player.AddItem(*this);
    }
    else {
        std::cout << "You leave the " << name << " alone" << std::endl;
    }



	
}

std::string Item::GetName() const
{
	return name;
}

std::string Item::GetDescription() const
{
	return description;
}
