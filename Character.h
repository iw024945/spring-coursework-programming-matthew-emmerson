#pragma once
#include <iostream>
#include <string>
#include <vector>
#include "Item.h"
#include "Room.h"
#include "Weapon.h"
#include "Quest.h"
class Character {


private:
	std::string name;

	std::vector<Item> inventory;
	
public:
	int health;
	Character(const std::string& name, int health); // Declaration
	void TakeDamage(int damage);
	void AddItem(const Item& item);
	void Move(const std::string& direction);
	Room* location;
	void combat(Character& opponent);
	void equipWeapon(Weapon* weapon);
	Weapon* weapon;
	std::string getName() const {
		return name;
	}
	

};
class Player : public Character {

private:
	std::vector<Quest> quests;


public:
	Player(const std::string& name, int health);
	void SetLocation(Room* room);
    Room* GetLocation();
	std::vector<Quest>& getQuests() {
		return quests;
	}
	void addQuest(const Quest& newQuest) {
		quests.push_back(newQuest);
	}

};
class Enemy : public Character {
public:
	Enemy(const std::string& name, int health, Weapon* weapon) : Character(name, health) {

		this->equipWeapon(weapon);

	}
};
class Warrior : public Player {
public:
	Warrior(const std::string& name, int health) : Player(name, health) {}

	void PowerStrike(Character& opponent);
};

class Mage : public Player {
public:
	Mage(const std::string& name, int health) : Player(name, health) {}
	void Spell(Character& opponent);
};

class Rogue : public Player {
public:
	Rogue(const std::string& name, int health) : Player(name, health) {}

	void SneakStrike(Character& opponent);
};



