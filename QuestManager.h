#pragma once
#include <vector>
#include "Quest.h"
class QuestManager {
public:
    std::vector<Quest> quests;

    void addQuest(const Quest& quest) {
        quests.push_back(quest);
    }

    void updateQuests() {
        for (Quest& quest : quests) {
            quest.update();
        }
    }
};


